package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
)

/*
 * https://thycotic.com/about-us/careers/code-example-2/
 * Challenge: Modify the go code below to calculate the average temperature in the 100 largest cities in the United States at the current time.
 * Handle any errors if a city is missing temperature data and skip that city in the final calculation.
 */

// Coordinate represents a geo location
type Coordinate struct {
	Latitude  float64
	Longitude float64
}

func (c Coordinate) String() string {
	return fmt.Sprintf("Coordinate{Latitude: %f, Longitude: %f}", c.Latitude, c.Longitude)
}

const weatherCityURLTemplate string = "https://www.metaweather.com/api/location/search/?lattlong=%f,%f"
const weatherURLTemplate string = "https://www.metaweather.com/api/location/%d/%d/%d/%d"
const citiesURLTemplate string = "https://examples.opendatasoft.com/api/records/1.0/search/?dataset=largest-us-cities&rows=%d&sort=population&facet=city&facet=state"
const numberOfLargestCities = 100

func main() {
	rand.Seed(time.Now().UnixNano())

	cities, err := getLargestNCities(numberOfLargestCities)
	checkErr(err)

	largestCitiesCoordinates := getCoordinatesForLargestNCities(cities, numberOfLargestCities)

	log.Printf("Average temperature in the 100 largest cities in the United States at the current time: %f\n",
		calculateAverageTemperature(largestCitiesCoordinates))
}

func calculateAverageTemperature(coordinates []Coordinate) float64 {
	temperatures := make([]float64, len(coordinates))
	for i, coordinate := range coordinates {
		temperature, err := getCurrentTemperatureForCoordinate(coordinate)
		if err != nil {
			log.Println(err)
		} else {
			log.Printf("[%d] Temperature at coordinate: %s is %f\n", i+1, coordinate, temperature)
		}
		temperatures[i] = temperature

		nap()
	}

	return avgFloats64(temperatures)
}

func getCurrentTemperatureForCoordinate(coord Coordinate) (float64, error) {
	weatherCityWoeid, err := getWeatherStationIDForCoordinate(coord)
	if err != nil {
		return 0, err
	}

	weatherURLFormatted := fmt.Sprintf(weatherURLTemplate, weatherCityWoeid,
		time.Now().Year(), int(time.Now().Month()), time.Now().Day())
	log.Printf("Fetching weather data for coordinate: %s from: %s\n", coord, weatherURLFormatted)
	weatherData, err := doGetRequest(weatherURLFormatted)
	if err != nil {
		return 0, err
	}

	weatherDataParsed, _ := gabs.ParseJSON(weatherData)
	weatherReports := weatherDataParsed.Children()
	if len(weatherReports) == 0 {
		return 0, fmt.Errorf("no weather report found for station with ID: %d at location: %v", weatherCityWoeid, coord)
	}

	temperature, ok := weatherReports[0].Path("the_temp").Data().(float64)
	if !ok {
		log.Printf("No temperature for coordinate: %s\n", coord)
	}
	return temperature, nil
}

func getWeatherStationIDForCoordinate(coord Coordinate) (int64, error) {
	weatherStationURL := fmt.Sprintf(weatherCityURLTemplate, coord.Latitude, coord.Longitude)
	log.Printf("Fetching weather station for coordinate: %s from: %s\n", coord, weatherStationURL)
	weatherCityData, err := doGetRequest(weatherStationURL)
	if err != nil {
		return -1, err
	}

	weatherCitiesParsed, err := gabs.ParseJSON(weatherCityData)
	if err != nil {
		return -1, err
	}

	weatherStationsAtLocation := weatherCitiesParsed.Children()
	if len(weatherStationsAtLocation) == 0 {
		return -1, fmt.Errorf("no weather station found nearby coordinate: %v", coord)
	}

	return int64(weatherStationsAtLocation[0].Path("woeid").Data().(float64)), nil
}

func getLargestNCities(n int) ([]*gabs.Container, error) {
	cityData, err := doGetRequest(fmt.Sprintf(citiesURLTemplate, n))
	if err != nil {
		return nil, err
	}

	cityDataParsed, err := gabs.ParseJSON(cityData)
	if err != nil {
		return nil, err
	}

	return cityDataParsed.Path("records").Children(), nil
}

func sortCitiesDescendingByPopulation(cities []*gabs.Container) {
	sort.Slice(cities, func(i, j int) bool {
		cityAPopulation := cities[i].Path("fields.population").Data().(float64)
		cityBPopulation := cities[j].Path("fields.population").Data().(float64)
		return cityAPopulation > cityBPopulation
	})
}

func getCoordinatesForLargestNCities(cities []*gabs.Container, n int) []Coordinate {
	cityCoordinates := make([]Coordinate, n)
	for i, city := range cities {
		if i >= n {
			break
		}

		coordRaw := city.Path("fields.coordinates").Data().(string)
		coord := strings.Split(coordRaw, ";")
		if len(coord) == 1 {
			log.Printf("Skipping invalid coordinate: %s\n", coordRaw)
			continue
		}

		latitude, err := strconv.ParseFloat(coord[0], 64)
		if err != nil {
			log.Printf("Invalid coordinate latitude: %s, skipping...\n", coord[0])
			continue
		}
		longitude, err := strconv.ParseFloat(coord[1], 64)
		if err != nil {
			log.Printf("Invalid coordinate longitude: %s, skipping...\n", coord[1])
			continue
		}

		log.Printf("[%d] City: %s, Population: %d\n", i+1, city.Path("fields.city").Data().(string), int64(city.Path("fields.population").Data().(float64)))

		cityCoordinates[i] = Coordinate{
			Latitude:  latitude,
			Longitude: longitude,
		}
	}
	return cityCoordinates
}

func doGetRequest(url string) ([]byte, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func sumFloats64(floats []float64) float64 {
	sum := 0.0
	for _, f := range floats {
		sum += f
	}
	return sum
}

func avgFloats64(floats []float64) float64 {
	count := float64(len(floats))
	sum := sumFloats64(floats)
	return sum / count
}

func getRandomIntInRange(min, max int) int {
	return rand.Intn(max-min+1) + min
}

func nap() {
	// Precautionary delay between requests to prevent server from rate limiting or blocking us
	duration := time.Duration(getRandomIntInRange(0, 5)) * time.Second
	log.Printf("Napping for: %s\f", duration)
	time.Sleep(duration)
}
